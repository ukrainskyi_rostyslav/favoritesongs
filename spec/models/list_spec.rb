require 'rails_helper'

describe List do
  before(:each) do
    factory_user = FactoryBot.create(:user)
    @attr = { name: 'Example',
              user_id: factory_user.id }
  end

  it 'should create a new instance given valid attributes' do
    new_list = List.create(@attr)
    new_list.should be_valid
  end

  it 'should lists name' do
    new_list = List.create(@attr.merge(name: ''))
    new_list.should_not be_valid
  end

  it 'should lists user' do
    new_list = List.create(@attr.merge(user_id: ''))
    new_list.should_not be_valid
  end
end
