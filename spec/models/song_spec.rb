require 'rails_helper'

describe Song do
  before(:each) do
    factory_user = FactoryBot.create(:user)
    factory_style = FactoryBot.create(:style)
    factory_list = FactoryBot.create(:list, user_id: factory_user.id)
    @attr = { name: 'Example',
              author: 'New Example',
              list_id: factory_list.id,
              style_id: factory_style.id }
  end

  it 'should create a new instance given valid attributes' do
    new_song = Song.create(@attr)
    new_song.should be_valid
  end

  it 'should songs name' do
    new_song = Song.create(@attr.merge(name: ''))
    new_song.should_not be_valid
  end

  it 'should songs author' do
    new_song = Song.create(@attr.merge(author: ''))
    new_song.should_not be_valid
  end

  it 'should songs list' do
    new_song = Song.create(@attr.merge(list_id: ''))
    new_song.should_not be_valid
  end

  it 'should songs style' do
    new_song = Song.create(@attr.merge(style_id: ''))
    new_song.should_not be_valid
  end
end
