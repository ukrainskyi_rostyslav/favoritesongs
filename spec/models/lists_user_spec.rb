require 'rails_helper'

describe ListsUser do
  before(:each) do
    factory_user = FactoryBot.create(:user)
    factory_user_2 = FactoryBot.create(:user)
    factory_list = FactoryBot.create(:list, user_id: factory_user.id)
    @attr = { list_id: factory_list.id,
              user_id: factory_user_2.id }
  end

  it 'should create a new instance given valid attributes' do
    new_lists_user = ListsUser.create(@attr)
    new_lists_user.should be_valid
  end

  it 'should users list' do
    new_lists_user = ListsUser.create(@attr.merge(list_id: ''))
    new_lists_user.should_not be_valid
  end

  it 'should users user' do
    new_lists_user = ListsUser.create(@attr.merge(user_id: ''))
    new_lists_user.should_not be_valid
  end
end
