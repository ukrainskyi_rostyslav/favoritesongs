require 'rails_helper'

describe StyleRating do
  before(:each) do
    factory_style = FactoryBot.create(:style)
    @attr = { style_id: factory_style.id }
  end

  it 'should create a new instance given valid attributes' do
    new_style_rating = StyleRating.create(@attr)
    new_style_rating.should be_valid
  end

  it 'should style' do
    new_style_rating = StyleRating.create(style_id: '')
    new_style_rating.should_not be_valid
  end
end
