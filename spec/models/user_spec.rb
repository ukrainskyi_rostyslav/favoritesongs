require 'rails_helper'

describe User do
  before(:each) do
    @attr = { email: 'example@example.com',
              password: 'newexample' }
  end

  it 'should create a new instance given valid attributes' do
    new_user = User.create(@attr)
    new_user.should be_valid
  end

  it 'should users email' do
    new_user = User.create(@attr.merge(email: ''))
    new_user.should_not be_valid
  end

  it 'should users password' do
    new_user = User.create(@attr.merge(password: ''))
    new_user.should_not be_valid
  end
end
