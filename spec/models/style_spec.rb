require 'rails_helper'

describe Style do
  before(:each) do
    @attr = { name: 'example' }
  end

  it 'should create a new instance given valid attributes' do
    new_style = Style.create(@attr)
    new_style.should be_valid
  end

  it 'should users style' do
    new_style = Style.create(name: '')
    new_style.should_not be_valid
  end
end
