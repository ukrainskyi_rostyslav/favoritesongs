require 'rails_helper'

describe Api::V1::ListsUsersController do
  before(:each) do
    @factory_user = FactoryBot.create(:user)
    @factory_user_2 = FactoryBot.create(:user)
    @factory_user_3 = FactoryBot.create(:user)
    @factory_list = FactoryBot.create(:list, user_id: @factory_user.id)
    @factory_list_2 = FactoryBot.create(:list, is_private: false, user_id: @factory_user.id)
    ListsUser.create(user_id: @factory_user_2.id, list_id: @factory_list.id)
  end

  describe "POST 'create' " do
    context 'correct list and user format' do
      it 'returns a successful json string with success message' do
        post :create, {
          user_token: @factory_user.authentication_token,
          lists_user: { user_id: @factory_user_3.id, list_id: @factory_list.id }
        }
        expect(response).to have_http_status(201)
      end
    end

    context 'incorrect list format' do
      it 'returns an error if an incorrect list_id' do
        post :create, {
          user_token: @factory_user.authentication_token,
          lists_user: { user_id: @factory_user_3.id, list_id: '' }
        }
        expect(response).to have_http_status(400)
      end
    end

    context 'incorrect list public format' do
      it 'returns an error that list is public' do
        post :create, {
          user_token: @factory_user.authentication_token,
          lists_user: { user_id: @factory_user_3.id, list_id: @factory_list_2.id }
        }
        expect(response).to have_http_status(400)
      end
    end

    context 'incorrect user format' do
      it 'returns an error if an incorrect user_id' do
        post :create, {
          user_token: @factory_user_3.authentication_token,
          lists_user: { user_id: '', list_id: @factory_list.id }
        }
        expect(response).to have_http_status(400)
      end
    end

    context 'incorrect with save' do
      it 'returns an error that error with save' do
        post :create, {
          user_token: @factory_user.authentication_token,
          lists_user: { user_id: @factory_user_2.id, list_id: @factory_list.id }
        }
        expect(response).to have_http_status(400)
      end
    end
  end
end
