require 'rails_helper'

describe Api::V1::SongsController do
  before(:each) do
    @factory_user = FactoryBot.create(:user)
    @factory_list = FactoryBot.create(:list, user_id: @factory_user.id)
    @factory_style = FactoryBot.create(:style)
    @factory_song = FactoryBot.create(:song, list_id: @factory_list.id, style_id: @factory_style.id)
  end

  describe "GET 'index' " do
    context 'correct song and list' do
      it 'returns a successful json string with success message' do
        get :index, {
          user_token: @factory_user.authentication_token,
          song: { type: 'date', list_id: @factory_list.id }
        }
        expect(response).to have_http_status(200)
      end
    end

    context 'correct user and list' do
      it 'returns a successful json string with success message' do
        get :index, {
          user_token: @factory_user.authentication_token,
          song: { type: 'style', list_id: @factory_list.id }
        }
        expect(response).to have_http_status(200)
      end
    end

    context 'correct user and list' do
      it 'returns a successful json string with success message' do
        get :index, {
          user_token: @factory_user.authentication_token,
          song: { type: 'name', list_id: @factory_list.id }
        }
        expect(response).to have_http_status(200)
      end
    end

    context 'correct user and list' do
      it 'returns a successful json string with success message' do
        get :index, {
          user_token: @factory_user.authentication_token,
          song: { page: '1', list_id: @factory_list.id }
        }
        expect(response).to have_http_status(200)
      end
    end

    context 'correct user and list' do
      it 'returns a successful json string with success message' do
        get :index, {
          user_token: @factory_user.authentication_token,
          song: { list_id: @factory_list.id }
        }
        expect(response).to have_http_status(200)
      end
    end

    context 'correct user and list' do
      it 'returns an error if an incorrect list_id' do
        get :index, {
          user_token: @factory_user.authentication_token,
          song: { list_id: '' }
        }
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "POST 'create' " do
    context 'correct list and song format' do
      it 'returns a successful json string with success message' do
        post :create, {
          user_token: @factory_user.authentication_token,
          song: { name: @factory_song.name, author: @factory_song.author,
                  list_id: @factory_list.id, style_id: @factory_style.id }
        }
        expect(response).to have_http_status(:success)
      end
    end

    context 'incorrect email format' do
      it 'returns an error if an incorrect song name' do
        post :create, {
          user_token: @factory_user.authentication_token,
          song: { name: '', author: @factory_song.author, list_id: @factory_list.id }
        }
        expect(response).to have_http_status(400)
      end
    end

    context 'incorrect email format' do
      it 'returns an error if an incorrect song author' do
        post :create, {
          user_token: @factory_user.authentication_token,
          song: { name: @factory_song.name, author: '', list_id: @factory_list.id }
        }
        expect(response).to have_http_status(400)
      end
    end

    context 'incorrect email format' do
      it 'returns an error if an incorrect list_id' do
        post :create, {
          user_token: @factory_user.authentication_token,
          song: { list_id: '' }
        }
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "PUT 'update' " do
    context 'correct user and list' do
      it 'returns a successful json string with success message' do
        put :update, {
          user_token: @factory_user.authentication_token,
          song: { id: @factory_song.id, name: @factory_song.name, author: @factory_song.author }
        }
        expect(response).to have_http_status(200)
      end
    end

    context 'incorrect name format' do
      it 'returns an error if an incorrect name' do
        put :update, {
          user_token: @factory_user.authentication_token,
          song: { id: @factory_song.id, name: '', author: @factory_song.author }
        }
        expect(response).to have_http_status(400)
      end
    end

    context 'incorrect author format' do
      it 'returns an error if an incorrect author' do
        put :update, {
          user_token: @factory_user.authentication_token,
          song: { id: @factory_song.id, name: @factory_song.name, author: '' }
        }
        expect(response).to have_http_status(400)
      end
    end

    context 'incorrect song format' do
      it 'returns an error if an incorrect song_id' do
        put :update, {
          user_token: @factory_user.authentication_token,
          song: { id: '' }
        }
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "DELETE 'destroy' " do
    context 'successful destroy' do
      it 'returns a successful json string with success message' do
        delete :destroy, {
          user_token: @factory_user.authentication_token,
          song: { id: @factory_song.id }
        }
        expect(response).to have_http_status(200)
      end
    end

    context 'unsuccessful destroy' do
      it 'returns an error if an incorrect song_id' do
        delete :destroy, {
          user_token: @factory_user.authentication_token,
          song: { id: '' }
        }
        expect(response).to have_http_status(404)
      end
    end
  end
end
