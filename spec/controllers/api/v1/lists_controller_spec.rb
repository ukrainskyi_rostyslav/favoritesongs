require 'rails_helper'

describe Api::V1::ListsController do
  before(:each) do
    @factory_user = FactoryBot.create(:user)
    @factory_list = FactoryBot.create(:list, user_id: @factory_user.id)
  end

  describe "POST 'create' " do
    context 'correct user and list' do
      it 'returns a successful json string with success message' do
        post :create, {
          user_token: @factory_user.authentication_token,
          list: { name: @factory_list.name, user_id: @factory_user.id }
        }, headers: {}
        expect(response).to have_http_status(201)
      end
    end

    context 'incorrect name format' do
      it 'returns an error if an incorrect name' do
        post :create, {
          user_token: @factory_user.authentication_token,
          list: { name: '', user_id: @factory_user.id }
        }
        expect(response).to have_http_status(400)
      end
    end
  end

  describe "PUT 'update' " do
    context 'correct user and list' do
      it 'returns a successful json string with success message' do
        put :update, {
          user_token: @factory_user.authentication_token,
          list: { name: 'qwerty', id: @factory_list.id }
        }
        expect(response).to have_http_status(200)
      end
    end

    context 'incorrect name format' do
      it 'returns an error if an incorrect name' do
        put :update, {
          user_token: @factory_user.authentication_token,
          list: { name: '', id: @factory_list.id }
        }
        expect(response).to have_http_status(400)
      end
    end

    context 'incorrect list format' do
      it 'returns an error if an incorrect list_id' do
        put :update, {
          user_token: @factory_user.authentication_token,
          list: { id: '' }
        }
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "DELETE 'destroy' " do
    context 'correct user and list' do
      it 'returns a successful json string with success message' do
        delete :destroy, {
          user_token: @factory_user.authentication_token,
          list: { id: @factory_list.id }
        }
        expect(response).to have_http_status(200)
      end
    end

    context 'incorrect list format' do
      it 'returns an error if an incorrect list_id' do
        delete :destroy, {
          user_token: @factory_user.authentication_token,
          list: { id: '' }
        }
        expect(response).to have_http_status(404)
      end
    end
  end
end
