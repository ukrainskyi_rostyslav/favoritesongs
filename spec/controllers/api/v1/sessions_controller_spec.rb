require 'rails_helper'

describe Api::V1::SessionsController do
  before(:each) do
    @factory_user = FactoryBot.create(:user)
  end

  describe "POST 'create' " do
    context 'correct email or password format' do
      it 'returns a successful json string with success message' do
        post :create, {
          email: @factory_user.email, password: @factory_user.password
        }
        expect(response).to have_http_status(:success)
      end
    end

    context 'incorrect email format' do
      it 'returns an error if an incorrect email or password format is submitted' do
        post :create, {
          email: 'ukr@ukr.com', password: ''
        }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "DELETE 'destroy' " do
    context 'correct token' do
      it 'returns status 204 json nil' do
        delete :destroy, {
          user_token: @factory_user.authentication_token
        }
        expect(response).to have_http_status(204)
      end
    end

    context 'incorrect token' do
      it "returns status 404 with message 'Invalid token.'" do
        delete :destroy, {
          user_token: '0'
        }
        expect(response).to have_http_status(404)
      end
    end
  end
end
