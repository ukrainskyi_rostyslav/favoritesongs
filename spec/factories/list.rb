FactoryBot.define do
  factory :list do
    name { Faker::App.name }
    description { Faker::Hacker.say_something_smart }
  end
end
