FactoryBot.define do
  factory :song do
    name { Faker::App.name }
    author { Faker::Book.author }
    url { Faker::Internet.url }
  end
end
