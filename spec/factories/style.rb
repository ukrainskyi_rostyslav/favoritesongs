FactoryBot.define do
  factory :style do
    name { Faker::App.name }
  end
end
