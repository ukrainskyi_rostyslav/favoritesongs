Rails.application.routes.draw do
  devise_for :users
  root to: 'lists#index', as: 'home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :sessions, only: %i[create destroy], param: :user_token
      resources :lists, only: %i[create index destroy update], param: :user_token
      resources :songs, only: %i[create index destroy update], param: :user_token
      resources :lists_users, only: %i[create show destroy update], param: :user_token
    end
  end

  resources :songs, only: %i[index destroy]
  resources :lists, only: :destroy
  resources :search, only: :index
  resources :add_lists, only: :create
  resources :style_ratings, only: :create
end
