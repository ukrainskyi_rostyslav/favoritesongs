class List < ApplicationRecord
  validates :name, presence: true
  belongs_to :owner, class_name: User, foreign_key: 'user_id'
  has_many :songs, dependent: :destroy
  has_and_belongs_to_many :users
  validate :user_id_exists

  scope :available_users_lists, (lambda do |user_id|
    left_outer_joins(:users).where(
      '(lists.user_id = :user_id or lists_users.user_id = :user_id) OR is_private = :is_private',
      { user_id: user_id, is_private: false }
    )
  end)

  def user_id_exists
    errors.add(:user_id, 'User does not exist.') unless User.find_by(id: user_id)
  end

  def copy_songs(old_list)
    old_list.songs.each do |song|
      new_song = song.dup
      new_song.attributes = { id: nil, list_id: id }
      new_song.save
    end
  end

  def as_json(options = {})
    super(options.reverse_merge(only: %i[name description is_private]))
  end
end
