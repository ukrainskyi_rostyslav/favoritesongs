class Song < ApplicationRecord
  validates :name, :author, presence: true
  validate :list_id_exists
  validate :style_id_exists
  belongs_to :list
  belongs_to :style

  def list_id_exists
    errors.add(:list_id, 'List does not exist.') unless List.find_by(id: list_id)
  end

  def style_id_exists
    errors.add(:style_id, 'Style does not exist.') unless Style.find_by(id: style_id)
  end

  def as_json(options = {})
    json = super(options.reverse_merge(only: %i[name author url]))
    json[:style] = style.name
    json
  end
end
