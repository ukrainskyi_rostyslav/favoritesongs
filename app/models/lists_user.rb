class ListsUser < ApplicationRecord
  validate :has_one_or_more_duplicates, on: :create
  validate :user_id_exists
  validate :list_id_exists_public

  def user_id_exists
    errors.add(:user_id, 'User does not exist.') unless User.find_by(id: user_id)
  end

  def list_id_exists_public
    list = List.find_by(id: list_id)
    if list.nil?
      errors.add(:list_id, 'List does not exist.')
    elsif !list.is_private
      errors.add(:list_id, 'List is public.')
    end
  end

  def has_one_or_more_duplicates
    duplicates = ListsUser.where(user_id: user_id, list_id: list_id)
    errors.add(:base, 'A user has subscription') if duplicates.count.positive?
  end
end
