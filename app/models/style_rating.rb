class StyleRating < ApplicationRecord
  belongs_to :my_style, class_name: Style, foreign_key: 'style_id'
  validate :style_id_exists

  def style_id_exists
    errors.add(:style_id, 'Style does not exist!') unless Style.find_by(id: style_id)
  end
end
