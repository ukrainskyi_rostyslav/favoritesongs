class Style < ApplicationRecord
  validates :name, presence: true
  has_many :owned_ratings, class_name: StyleRating, dependent: :destroy
  has_many :songs
end
