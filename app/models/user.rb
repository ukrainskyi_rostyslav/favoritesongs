class User < ApplicationRecord
  acts_as_token_authenticatable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :lists
  has_many :owned_lists, class_name: List, dependent: :destroy

  def search_lists(search_term)
    available_lists.left_outer_joins(:songs, songs: :style).where(
      '(songs.name ILIKE :search_term OR lists.name ILIKE :search_term'\
      ' OR lists.description ILIKE :search_term OR songs.author ILIKE :search_term'\
      ' OR styles.name ILIKE :search_term)', search_term: "%#{search_term}%"
    ).group('lists.id').distinct
  end

  def available_lists
    List.available_users_lists(id)
  end
end
