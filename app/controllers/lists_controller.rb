class ListsController < ApplicationController
  before_action :retrieve_list, only: :destroy

  def index
    @lists =
      case params[:status]
      when 'private'
        Kaminari.paginate_array(current_user.owned_lists.where(is_private: true)
          .sort_by { |e| e[:created_at] }.reverse).page(params[:page]).per(8)
      when 'only'
        Kaminari.paginate_array(current_user.owned_lists
          .sort_by { |e| e[:created_at] }.reverse).page(params[:page]).per(8)
      else
        Kaminari.paginate_array((current_user.owned_lists + current_user.lists)
          .sort_by { |e| e[:created_at] }.reverse).page(params[:page]).per(8)
      end
    @lists_public = List.where(is_private: false).where.not(user_id: current_user.id).page(params[:page]).per(8)
    @styles = Style.all
    respond_to do |format|
      format.html {}
      format.json { render json: { html: render_to_string(partial: 'lists.slim', layout: false) } }
    end
  end

  def destroy
    authorize @list
    if @list.destroy
      redirect_to home_path, notice: 'The deletion was successful'
    else
      redirect_to home_path, notice: 'The deletion was unsuccessful'
    end
  end

  private

  def retrieve_list
    @list = List.find_by(id: params[:id])
    redirect_to(home_path, notice: 'List is not found.') && return if @list.blank?
  end
end
