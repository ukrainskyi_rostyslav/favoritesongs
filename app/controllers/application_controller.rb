class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :null_session
  before_action :authenticate_user!

  rescue_from Pundit::NotAuthorizedError do |exception|
    render json: { message: 'Not allowed!' }, status: 400
  end

end
