class StyleRatingsController < ApplicationController

  def create
    style_rating = StyleRating.new(style_rating_params)
    authorize style_rating
    redirect_to params[:url] if style_rating.save
  end

  private

  def style_rating_params
    { style_id: params[:style_id] }
  end
end
