class SongsController < ApplicationController
  before_action :retrieve_list, only: :index
  before_action :retrieve_song, only: :destroy

  def index
    @songs = Song.where(list_id: params[:list_id]).page(params[:page]).per(8)
    authorize @songs
    redirect_to home_path, notice: 'Songs are not found' if @songs.blank?
  end

  def destroy
    authorize @song
    if @song.destroy
      redirect_to songs_path(list_id: @song.list_id), notice: 'The deletion was successful'
    else
      redirect_to songs_path(list_id: @song.list_id), notice: 'The deletion was unsuccessful'
    end
  end

  private

  def retrieve_list
    @list = List.find_by(id: params[:list_id])
    redirect_to(home_path, notice: 'List is not found.') && return if @list.blank?
  end

  def retrieve_song
    @song = Song.find_by(id: params[:id])
    redirect_to(songs_path(list_id: @song.list_id), notice: 'Song is not found.') && return if @song.blank?
  end
end
