class Api::V1::ListsController < Api::ApiController
  before_action :set_user_by_token
  before_action :find_list, only: %i[update destroy]

  def index
    lists = List.where(user_id: @current_user.id)
    authorize lists
    unless lists.blank?
      render json: lists, status: 200
    else
      render json: { message: 'List not found' }, status: 404
    end
  end

  def create
    list = @current_user.owned_lists.new(list_params)
    authorize list
    if list.save
      render json: list, status: 201
    else
      render json: { message: 'Error with save' }, status: 400
    end
  end

  def update
    authorize @list
    if @list.update(list_params)
      render json: @list, status: 200
    else
      render json: { message: 'Error with update' }, status: 400
    end
  end

  def destroy
    authorize @list
    if @list.destroy
      render json: { message: 'Deletion completed successfully' }, status: 200
    else
      render json: { message: 'Error with deleted' }, status: 400
    end
  end

  private

  def find_list
    @list = List.find_by(id: params[:list][:id])
    render json: { message: 'List not found' }, status: 404 unless @list
  end

  def list_params
    params.require(:list).permit(:name, :description, :is_private)
  end
end
