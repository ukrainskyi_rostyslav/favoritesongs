class Api::V1::ListsUsersController < Api::ApiController
  before_action :set_user_by_token

  def create
    lists_user = ListsUser.new(lists_user_params)
    authorize lists_user
    if lists_user.save
      render json: { message: 'Successfully saved' }, status: 201
    else
      render json: { message: 'Error with save' }, status: 400
    end
  end

  private

  def lists_user_params
    params.require(:lists_user).permit(:user_id, :list_id)
  end
end
