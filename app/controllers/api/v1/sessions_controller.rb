class Api::V1::SessionsController < Api::ApiController
  before_action :set_user_by_token, only: :destroy

  def create
    user = User.find_by(email: params[:email])
    if user&.valid_password?(params[:password])
      render json: user.as_json(only: %i[email authentication_token]), status: 201
    else
      head(:unauthorized)
    end
  end

  def destroy
    @current_user.authentication_token = nil
    if @current_user.save
      render status: 204, json: nil
    else
      render json: { message: 'Error with save' }, status: 400
    end
  end
end
