class Api::V1::SongsController < Api::ApiController
  before_action :set_user_by_token
  before_action :find_song, only: %i[update destroy]

  def index
    songs = Song.where(list_id: params[:song][:list_id]).joins(:style)
    unless songs.blank?
      authorize songs
      songs =
        case params[:song][:type]
        when 'date'
          songs.order("created_at DESC")
        when 'style'
          songs.merge(Style.order(name: :asc))
        when 'name'
          songs.order("name ASC")
        else
          songs.order(:id)
        end
      songs = songs.page(params[:page] || 1).per(3)
      render json: songs, status: 200
    else
      render json: { message: 'Songs not found' }, status: 404
    end
  end

  def create
    list = @current_user.owned_lists.find_by(id: params[:song][:list_id])
    if list
      new_song = list.songs.new(song_params)
      authorize new_song
      if new_song.save
        render json: new_song, status: 201
      else
        render json: { message: 'Error with save' }, status: 400
      end
    else
      render json: { message: 'List not found' }, status: 404
    end
  end

  def update
    authorize @song
    if @song.update(song_params)
      render json: @song, status: 200
    else
      render json: { message: 'Error with update' }, status: 400
    end
  end

  def destroy
    authorize @song
    if @song.destroy
      render json: { message: 'Deletion completed successfully' }, status: 200
    else
      render json: { message: 'Error with deleted' }, status: 400
    end
  end

  private

  def find_song
    @song = Song.find_by(id: params[:song][:id])
    render json: { message: 'Song not found' }, status: 404 unless @song
  end

  def song_params
    params.require(:song).permit(:name, :author, :style_id, :url)
  end
end
