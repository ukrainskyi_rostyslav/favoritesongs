class Api::ApiController < ApplicationController
  protect_from_forgery
  skip_before_action :authenticate_user!

  private

  def set_user_by_token
    @current_user = User.find_by(authentication_token: params[:user_token])
    return render json: { message: 'Invalid token.' }, status: 404 unless @current_user
  end
end
