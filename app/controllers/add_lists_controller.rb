class AddListsController < ApplicationController
  before_action :retrieve_list

  def create
    new_list = current_user.owned_lists.new(add_list_params)
    authorize new_list
    if new_list.save
      new_list.copy_songs(@list)
      redirect_to home_path, notice: 'The addition was successful'
    else
      redirect_to home_path, notice: 'The addition was unsuccessful'
    end
  end

  private

  def retrieve_list
    @list = List.find_by(id: params[:format])
    redirect_to(home_path, notice: 'List is not found.') && return if @list.blank?
  end

  def add_list_params
    { name: @list.name, description: @list.description }
  end
end
