class SearchController < ApplicationController

  def index
    @lists = current_user.search_lists(params[:search]).page(params[:page]).per(8)
    authorize @lists
  end
end
