class ListsUserPolicy < ApplicationPolicy

  def create?
    return true if user.present? && user.id != lists_user.user_id
  end

  private

  def lists_user
    record
  end
end
