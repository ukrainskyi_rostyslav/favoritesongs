class SongPolicy < ApplicationPolicy
  
  def index?
    return true if user.present?
  end

  def create?
    return true if user.present? && user.id == song.list.user_id
  end

  def update?
    return true if user.present? && user.id == song.list.user_id
  end

  def destroy?
    return true if user.present? && user.id == song.list.user_id
  end

  private

  def song
    record
  end
end
