class ListPolicy < ApplicationPolicy

  def index?
    true
  end

  def create?
    user.present?
  end

  def update?
    return true if user.present? && user.id == list.user_id
  end

  def destroy?
    return true if user.present? && user.id == list.user_id
  end

  private

  def list
    record
  end
end
