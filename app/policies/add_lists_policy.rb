class AddListsPolicy < ApplicationPolicy
  
  def create?
    return true if user.present? && user.id != new_list.user_id
  end

  private
 
  def new_list
    record
  end
end