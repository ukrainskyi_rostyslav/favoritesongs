class CreateLists < ActiveRecord::Migration[5.0]
  def change
    create_table :lists do |t|
      t.string :name, null: false, default: ""
      t.text :description
      t.references :user, foreign_key: true
      t.boolean :is_private, default: "true"

      t.timestamps
    end
  end
end
