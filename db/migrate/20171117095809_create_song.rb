class CreateSong < ActiveRecord::Migration[5.0]
  def change
    create_table :songs do |t|
      t.string :name, null: false, default: ""
      t.string :author, null: false, default: ""
      t.references :style, foreign_key: true
      t.references :list, foreign_key: true
      t.string :url

      t.timestamps
    end
  end
end
