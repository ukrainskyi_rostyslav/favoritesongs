class CreateStyleRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :style_ratings do |t|
      t.references :style, null: false, foreign_key: true

      t.timestamps
    end
  end
end
