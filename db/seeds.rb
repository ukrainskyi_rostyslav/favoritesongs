# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create([{ email: 'ukr@ukr.com', password: '123456789' }])
User.create([{ email: 'lol@lol.com', password: '123456789' }])
p "Created #{User.count} users"

List.create(name: 'Cool first', description: 'This is the first list', user_id: 1)
List.create(name: 'Cool second', description: 'This is the second list', user_id: 1, is_private: false)
List.create(name: 'Cool third', description: 'This is the third list', user_id: 2)
List.create(name: 'Cool fourth', description: 'This is the fourth list', user_id: 2, is_private: false)
p "Created #{List.count} lists"

Style.create([{ name: 'Rock' }, { name: 'Jazz' },
              { name: 'Pop' }, { name: 'Folk' },
              { name: 'Hip hop' }, { name: 'Blues' }])
p "Created #{Style.count} styles"

Song.create(name: 'Fire', author: 'Kasabian',
            url: 'https://www.youtube.com/watch?v=agVpq_XXRmU',
            list_id: 1, style_id: 1)
Song.create(name: 'LSF', author: 'Kasabian',
            url: 'https://www.youtube.com/watch?v=yYYp5-mGQRI',
            list_id: 1, style_id: 1)
Song.create(name: 'Club Foot', author: 'Kasabian',
            url: 'https://www.youtube.com/watch?v=lk5iMgG-WJI',
            list_id: 1, style_id: 1)
Song.create(name: 'Underdog', author: 'Kasabian',
            url: 'https://www.youtube.com/watch?v=Gw09tAcNB0Q',
            list_id: 1, style_id: 1)
Song.create(name: 'Bonjour Sourire', author: 'Avalon Jazz Band',
            url: 'https://www.youtube.com/watch?v=BWiM3yYRa4s',
            list_id: 2, style_id: 2)
Song.create(name: 'Zou bisou bisou', author: 'Avalon Jazz Band',
            url: 'https://www.youtube.com/watch?v=NEnnysKyVfM',
            list_id: 2, style_id: 2)
Song.create(name: 'Jai ta main', author: 'Avalon Jazz Band',
            url: 'https://www.youtube.com/watch?v=HBkNC28yfLs',
            list_id: 2, style_id: 2)
Song.create(name: 'Cool first', author: 'Avalon Jazz Band',
            url: '', list_id: 2, style_id: 2)
Song.create(name: 'Thriller', author: 'Michael Jackson',
            url: 'https://www.youtube.com/watch?v=sOnqjkJTMaA',
            list_id: 3, style_id: 3)
Song.create(name: 'Beat It', author: 'Michael Jackson',
            url: 'https://www.youtube.com/watch?v=oRdxUFDoQe0',
            list_id: 3, style_id: 3)
Song.create(name: 'Billie Jean', author: 'Michael Jackson',
            url: 'https://www.youtube.com/watch?v=Zi_XLOBDo_Y',
            list_id: 3, style_id: 3)
Song.create(name: 'Smooth Criminal', author: 'Michael Jackson',
            url: 'https://www.youtube.com/watch?v=h_D3VFfhvs4',
            list_id: 3, style_id: 3)
Song.create(name: 'Flat Foot Sam', author: 'The Blues Band',
            url: 'https://www.youtube.com/watch?v=HwTXvO1kX_4',
            list_id: 4, style_id: 6)
Song.create(name: 'Dust My Blues', author: 'The Blues Band',
            url: 'https://www.youtube.com/watch?v=Wh1CAp5gAx8',
            list_id: 4, style_id: 6)
Song.create(name: 'I Am The Blues', author: 'The Blues Band',
            url: 'https://www.youtube.com/watch?v=Xl-ZOtbtG4I',
            list_id: 4, style_id: 6)
Song.create(name: 'Let The Good Times Roll', author: 'The Blues Band',
            url: 'https://www.youtube.com/watch?v=MM98b3MwUUI',
            list_id: 4, style_id: 6)
p "Created #{Song.count} songs"

StyleRating.create(style_id: 1, created_at: '2017-10-28 13:19:53')
StyleRating.create(style_id: 1, created_at: '2017-11-09 13:19:53')
StyleRating.create(style_id: 1, created_at: '2017-11-19 13:19:53')
StyleRating.create(style_id: 2, created_at: '2017-10-25 13:19:53')
StyleRating.create(style_id: 2, created_at: '2017-11-10 13:19:53')
StyleRating.create(style_id: 5, created_at: '2017-11-11 13:19:53')
StyleRating.create(style_id: 3, created_at: '2017-11-12 13:19:53')
p "Created #{StyleRating.count} ratings"
